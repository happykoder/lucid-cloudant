# Lucid Cloudant

lucid-cloudant is heavily inspired by `lucid-mongo` and it extends @adonis/lucid with the option to run it agains Cloudant

## Features

## <a name="getting-started"></a>Installation

## Use with AdonisJS framework

Install npm using `adonis` command.

```js
npm i --save git+http://<<git repo url>> 
```

Make sure to register the lucid provider to make use of `Database` and `LucidCloudant` models. The providers are registered inside `start/app.js`

```js
const providers = [
  // ...
  'lucid-cloudant/providers/LucidCloudantProvider'
]

const aceProviders = [
  // ...
  'lucid-cloudant/providers/MigrationsProvider'
]
```
the config automatic create to `config/database.js` file

```js
module.exports = {

  /*
  |--------------------------------------------------------------------------
  | Default Connection
  |--------------------------------------------------------------------------
  |
  | Connection defines the default connection settings to be used while
  | interacting with Cloudant databases.
  |
  */
  connection: Env.get('DB_CONNECTION', 'cloudant'),
  /*-------------------------------------------------------------------------*/

  cloudant: {
    client: 'cloudant',
    connectionString: Env.get('DB_CONNECTION_STRING', ''),
    connection: {
      host: Env.get('DB_HOST', 'localhost'),
      port: Env.get('DB_PORT', 27017),
      username: Env.get('DB_USER', 'admin'),
      password: Env.get('DB_PASSWORD', ''),
      database: Env.get('DB_DATABASE', 'adonis'),
      options: {
        // replicaSet: Env.get('DB_REPLICA_SET', '')
        // ssl: Env.get('DB_SSL, '')
        // connectTimeoutMS: Env.get('DB_CONNECT_TIMEOUT_MS', 15000),
        // socketTimeoutMS: Env.get('DB_SOCKET_TIMEOUT_MS', 180000),
        // w: Env.get('DB_W, 0),
        // readPreference: Env.get('DB_READ_PREFERENCE', 'secondary'),
        // authSource: Env.get('DB_AUTH_SOURCE', ''),
        // authMechanism: Env.get('DB_AUTH_MECHANISM', ''),
        // other options
      }
    }
  }
}
```

### Configuring Auth serializer
Edit the config/auth.js file for including the serializer. For example on the api schema
```js
  session: {
    serializer: 'LucidCloudant',
    model: 'App/Models/User',
    scheme: 'session',
    uid: 'email',
    password: 'password'
  },
  
  basic: {
    serializer: 'LucidCloudant',
    model: 'App/Models/User',
    scheme: 'basic',
    uid: 'email',
    password: 'password'
  },

  jwt: {
    serializer: 'LucidCloudant',
    model: 'App/Models/User',
    token: 'App/Models/Token',
    scheme: 'jwt',
    uid: 'email',
    password: 'password',
    expiry: '20m',
    options: {
      secret: 'self::app.appKey'
    }
  },

  api: {
    serializer: 'LucidCloudant',
    scheme: 'api',
    model: 'App/Models/User',
    token: 'App/Models/Token',
    uid: 'username',
    password: '',
    expiry: '30d',
  },
```


### Query

```js
const users =  await User.all()

const users =  await User.where('name', 'peter').fetch()

const users =  await User.where({ name: 'peter' })
  .limit(10).skip(20).fetch()

const users =  await User.where({
  or: [
    { gender: 'female', age: { gte: 20 } }, 
    { gender: 'male', age: { gte: 22 } }
  ]
}).fetch()

const user =  await User
  .where('name').eq('peter')
  .where('age').gt(18).lte(60)
  .sort('-age')
  .first()

const users =  await User
  .where({ age: { gte: 18 } })
  .sort({age: -1})
  .fetch()

const users =  await User
  .where('age', '>=', 18)
  .fetch()

const users =  await User
  .where('age').gt(18)
  .paginate(2, 100)

const users =  await User.where(function() {
  this.where('age', '>=', 18)
}).fetch()


// to query geo near you need declare field type as geometry and add 2d or 2dsphere index in migration file
const images = await Image.where({location: {near: {latitude: 1, longitude: 1}, maxDistance: 5000}}).fetch()

const images = await Image.where({location: {nearSphere: {latitude: 1, longitude: 1}, maxDistance: 500}}).fetch()
```
[More Documentation of mquery](https://github.com/aheckmann/mquery)

### Aggregation
```js
  // count without group by
  const count = await Customer.count()

  // count group by `position`
  const count_rows = await Customer
    .where({ invited: { $exist: true } })
    .count('position')

  // max age without group by
  const max = await Employee.max('age')

  // sum `salary` group by `department_id`
  const total_rows = await Employee
    .where(active, true)
    .sum('salary', 'department_id')

  // average group by `department_id` and `role_id`
  const avg_rows = await Employee
    .where(active, true)
    .avg('salary', { department: '$department_id', role: '$role_id' })
```

### Relations
This package support relations like adonis-lucid:
- hasOne
- belongsTo
- hasMany
- hasManyThrough
- belongsToMany

[More Documentation of adonis relationships](http://adonisjs.com/docs/4.0/relationships)


### Query relationships

```js
  const user = await User.with('emails').find(1)

  const user = await User.with('emails', query => query.where({ status: 'verified' })).find(1)

  const user = await User.with(['emails', 'phones']).find(1)

  const user = await User.with({ 
    email: {where: {verified: true}, sort: '-age'}
  }).find(1)

  const user = await User.with({email: query => {
    query.where(active, true)
  }}).find(1)

```

### Query logging
To show query logs run this command:
- Linux, MacOS `DEBUG=mquery npm run dev`
- Windows `setx DEBUG mquery && npm run dev`

### Migration
No support at the moment... 

### Field type
> Type of `date`
```js
class Staff extends LucidCloudant {
  static get dates() { return ['dob'] }
}
```
The field declare as date will be converted to moment js object after get from db
```js
const staff = await Staff.first()
const yearAgo = staff.dob.fromNow()
```
You can set attribute of model as moment|Date|string, this field will be converted to date before save to db
```js
staff.dob = moment(request.input('dob'))
```
The where query conditions will be converted to date too
```js
const user = await User
  .where({ created_at: { gte: '2017-01-01' } })
  .fetch()
```
Date type is UTC timezone
> Type of `geometry`
```js
class Image extends LucidCloudant {
  static get geometries() { return ['location'] }
}
```
When declare field type as geometry the field will be transformed to geoJSON type

```js
const image = await Image.create({
  fileName: fileName,
  location: {
    latitude: 1,
    longitude: 2
  }
})
```
Result:
```json
{ "type" : "Point", "coordinates" : [ 2, 1 ] }
```
After get from db it will be retransformed to 
```js
{
  latitude: 1,
  longitude: 2
}
```

### Use query builder
```js
  const Database = use('Database')
  const db = await Database.connection('cloudant')

  const users = await db.collection('users').find()

  const phone = await db.collection('phones')
    .where({userId: '58ccb403f895502b84582c63'}).findOne()
    
  const count = await db.collection('user')
    .where({active: true}).count()
```